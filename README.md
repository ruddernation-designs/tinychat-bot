This is a bot to use in your Tinychat room,

It's an aid to help moderate a room from spam and bad users, It's also a tool for YouTube/Soundcloud,

You're able to add multiple songs to a playlist, This also has other features ranging from weather to yomama jokes,

This is using the pinylib library by @nortxort, You'll need all the dependencies for this to work,

You can install the modules directly in your python library or run them from the bot folder,

Just make sure you have [Python 2.7](https://www.python.org/downloads/) installed.

Please check the [commands](https://github.com/Tinychat/Tinychat-Bot/wiki) for the full list.

This has fixes from the old bot and improvements,

Moderators cannot ban another Moderator (I wasn't aware of this until a user (`Jesus`) informed me.)

Moderators now have better controls over the room for when the super user is not available,

Some of which are `op`, `deop`, `guests`, `guestnicks`, `badnicks`,`badaccounts` without the need for a key.

**I've also provided my own Api keys for YouTube, Soundcloud and the World Weather Online,**

**I would recommend that you register with each provider and use your own Api keys, The links are provided below.**

* [Google Developers](https://developers.google.com)

* [Soundcloud](https://developers.soundcloud.com)

* [World Weather Online](http://developer.worldweatheronline.com/api/)

##Windows
* `C:\Python27\Scripts\pip2 install bf4 pyamf requests colorama goslate` or

* `cd c:\Extracted folders directory\ python setup.py install` 

or

* open command prompt or Powershell and type `pip install bf4 pyamf requests colorama goslate`

##Linux
Open the terminal and type

* `pip2 install bf4 pyamf requests colorama goslate`

##Mac
Open the terminal and type

* `sudo easy_install pip` then

* `pip install bf4 pyamf requests colorama goslate`

##Help running Python and pip from inside command prompt or windows shell.
###To be able to run `pip` inside command prompt on Windows use the below info,

Open start, locate control panel and open it, click `System and Security`,

Then click `System`, on the Right click `Advanced system settings`,

Then click `Environment Variables`, In the System variables box scroll to Path and double click it,

It will open a box, Click `Edit` or `New` depending on your version of Windows,

Now type `C:\Python27\;C:\Python27\Scripts\;` Click Ok

Now open Command Prompt or Shell and type in `python` and click Enter it should now open python in the window,

Now you can run pip directly inside command prompt, `pip install bf4 pyamf requests colorama goslate`,

If this still isn't working for you, Please [contact](https://www.ruddernation.com) me for help.